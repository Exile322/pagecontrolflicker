object PersonnelDetailsForm: TPersonnelDetailsForm
  Left = 0
  Top = 0
  Caption = 'Personnel Details Form'
  ClientHeight = 371
  ClientWidth = 800
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnMouseWheel = FormMouseWheel
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 17
  object ScrollBox_Content: TScrollBox
    Left = 0
    Top = 0
    Width = 800
    Height = 371
    VertScrollBar.Smooth = True
    VertScrollBar.Tracking = True
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 795
    ExplicitHeight = 600
    object panel_AddressDetails: TPanel
      Tag = 101
      Left = 0
      Top = 0
      Width = 796
      Height = 174
      Align = alTop
      Padding.Left = 5
      Padding.Top = 5
      Padding.Right = 5
      Padding.Bottom = 5
      ParentBackground = False
      TabOrder = 0
      ExplicitWidth = 791
      object gpanel_Address: TGridPanel
        Left = 6
        Top = 30
        Width = 784
        Height = 138
        Align = alClient
        BevelOuter = bvNone
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 105.000000000000000000
          end
          item
            Value = 50.000762951094850000
          end
          item
            SizeStyle = ssAbsolute
            Value = 105.000000000000000000
          end
          item
            Value = 49.999237048905160000
          end>
        ControlCollection = <
          item
            Column = 3
            Control = edit_HomeMobilePhone
            Row = 1
          end
          item
            Column = 1
            Control = edit_HomeTown
            Row = 1
          end
          item
            Column = 1
            Control = edit_HomeStreet
            Row = 0
          end
          item
            Column = 3
            Control = edit_HomePhone
            Row = 0
          end
          item
            Column = 0
            Control = lbl_HomeStreet
            Row = 0
          end
          item
            Column = 2
            Control = lbl_HomePhone
            Row = 0
          end
          item
            Column = 2
            Control = lbl_MobilePhone
            Row = 1
          end
          item
            Column = 0
            Control = lbl_HomeTown
            Row = 1
          end
          item
            Column = 1
            Control = edit_HomeState
            Row = 2
          end
          item
            Column = 3
            Control = edit_HomeEmail
            Row = 2
          end
          item
            Column = 1
            Control = edit_HomeCountry
            Row = 3
          end
          item
            Column = 3
            Control = edit_HomeFax
            Row = 3
          end
          item
            Column = 0
            Control = lbl_HomeState
            Row = 2
          end
          item
            Column = 2
            Control = lbl_Fax
            Row = 3
          end
          item
            Column = 2
            Control = lbl_Email
            Row = 2
          end
          item
            Column = 0
            Control = lbl_HomeCountry
            Row = 3
          end
          item
            Column = 1
            Control = edit_HomePostCode
            Row = 4
          end
          item
            Column = 0
            Control = lbl_HomePostCode
            Row = 4
          end>
        Padding.Left = 1
        Padding.Top = 1
        Padding.Right = 1
        Padding.Bottom = 1
        RowCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 27.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 27.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 27.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 27.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 27.000000000000000000
          end>
        TabOrder = 0
        ExplicitWidth = 779
        object edit_HomeMobilePhone: TEdit
          Left = 498
          Top = 29
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 6
          Text = 'Mobile Phone'
          ExplicitLeft = 495
          ExplicitWidth = 282
        end
        object edit_HomeTown: TEdit
          Left = 107
          Top = 29
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 1
          Text = 'Home Town'
          ExplicitWidth = 281
        end
        object edit_HomeStreet: TEdit
          Left = 107
          Top = 2
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 0
          Text = 'Home Street'
          ExplicitWidth = 281
        end
        object edit_HomePhone: TEdit
          Left = 498
          Top = 2
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 5
          Text = 'Home Phone'
          ExplicitLeft = 495
          ExplicitWidth = 282
        end
        object lbl_HomeStreet: TLabel
          Left = 61
          Top = 2
          Width = 44
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Street: '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Layout = tlCenter
        end
        object lbl_HomePhone: TLabel
          Left = 408
          Top = 2
          Width = 88
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Home Phone: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lbl_MobilePhone: TLabel
          Left = 402
          Top = 29
          Width = 94
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Mobile Phone: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lbl_HomeTown: TLabel
          Left = 64
          Top = 29
          Width = 41
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Town: '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Layout = tlCenter
        end
        object edit_HomeState: TEdit
          Left = 107
          Top = 56
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 2
          Text = 'Home State'
          ExplicitWidth = 281
        end
        object edit_HomeEmail: TEdit
          Left = 498
          Top = 56
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 7
          Text = 'Home Email'
          ExplicitLeft = 495
          ExplicitWidth = 282
        end
        object edit_HomeCountry: TEdit
          Left = 107
          Top = 83
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 3
          Text = 'Home Country'
          ExplicitWidth = 281
        end
        object edit_HomeFax: TEdit
          Left = 498
          Top = 83
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 8
          Text = 'Home Fax'
          ExplicitLeft = 495
          ExplicitWidth = 282
        end
        object lbl_HomeState: TLabel
          Left = 66
          Top = 56
          Width = 39
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'State: '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Layout = tlCenter
        end
        object lbl_Fax: TLabel
          Left = 467
          Top = 83
          Width = 29
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Fax: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lbl_Email: TLabel
          Left = 454
          Top = 56
          Width = 42
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Email: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lbl_HomeCountry: TLabel
          Left = 47
          Top = 83
          Width = 58
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Country: '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Layout = tlCenter
        end
        object edit_HomePostCode: TEdit
          Left = 107
          Top = 110
          Width = 284
          Height = 25
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 4
          Text = 'Home Post Code'
          ExplicitWidth = 281
        end
        object lbl_HomePostCode: TLabel
          Left = 35
          Top = 110
          Width = 70
          Height = 17
          Align = alClient
          Alignment = taRightJustify
          Caption = 'Post Code: '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = True
          Layout = tlCenter
        end
      end
      object panel_HomeAddressTitle: TPanel
        Left = 6
        Top = 6
        Width = 784
        Height = 24
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = ' Home Address '
        Color = clMedGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold, fsUnderline]
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        ExplicitWidth = 779
      end
    end
    object panel_GeneralNotesDetails: TPanel
      Tag = 303
      Left = 0
      Top = 174
      Width = 796
      Height = 172
      Align = alTop
      AutoSize = True
      Padding.Left = 5
      Padding.Top = 5
      Padding.Right = 5
      Padding.Bottom = 5
      ParentBackground = False
      TabOrder = 1
      ExplicitTop = 384
      ExplicitWidth = 791
      object gpanel_GeneralNotesDetails_: TGridPanel
        Left = 6
        Top = 6
        Width = 784
        Height = 160
        Align = alTop
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = pageControl_GeneralNotes
            Row = 0
          end>
        Padding.Left = 1
        Padding.Top = 1
        Padding.Right = 1
        Padding.Bottom = 1
        RowCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 160.000000000000000000
          end>
        TabOrder = 0
        ExplicitWidth = 779
        object pageControl_GeneralNotes: TPageControl
          Left = 2
          Top = 2
          Width = 780
          Height = 158
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 775
        end
      end
    end
  end
end
