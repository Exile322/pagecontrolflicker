program PageControlFlicker;



uses
  Vcl.Forms,
  Personnel.DetailsForm in 'Personnel.DetailsForm.pas' {PersonnelDetailsForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TPersonnelDetailsForm, PersonnelDetailsForm);
  Application.Run;
end.
