unit Personnel.DetailsForm;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.Actions,
	Vcl.ActnList, Vcl.Buttons, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.WinXCtrls, Vcl.Imaging.jpeg;

type
	TPersonnelDetailsForm = class(TForm)
		ScrollBox_Content: TScrollBox;
		panel_AddressDetails: TPanel;
		gpanel_Address: TGridPanel;
		edit_HomeMobilePhone: TEdit;
		edit_HomeTown: TEdit;
		edit_HomeStreet: TEdit;
		edit_HomePhone: TEdit;
		lbl_HomeStreet: TLabel;
		lbl_HomePhone: TLabel;
		lbl_MobilePhone: TLabel;
		lbl_HomeTown: TLabel;
		edit_HomeState: TEdit;
		edit_HomeEmail: TEdit;
		edit_HomeCountry: TEdit;
		edit_HomeFax: TEdit;
		lbl_HomeState: TLabel;
		lbl_Fax: TLabel;
		lbl_Email: TLabel;
		lbl_HomeCountry: TLabel;
		edit_HomePostCode: TEdit;
		lbl_HomePostCode: TLabel;
		panel_HomeAddressTitle: TPanel;
		panel_GeneralNotesDetails: TPanel;
		gpanel_GeneralNotesDetails_: TGridPanel;
		pageControl_GeneralNotes: TPageControl;
		procedure FormClose(Sender: TObject; var Action: TCloseAction);
		procedure FormShow(Sender: TObject);
		procedure FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
	private
		{ Private declarations }
	public
		{ Public declarations }
	end;

var
	PersonnelDetailsForm: TPersonnelDetailsForm;

implementation

{$R *.dfm}

uses
	System.Math,
	System.DateUtils,
	System.Threading,
	System.RegularExpressions,
	System.StrUtils,
	System.Contnrs,
	System.UITypes,
	System.Types,

	Winapi.Shellapi,

	Vcl.ExtDlgs;

procedure EnableComposited(WinControl: TWinControl);
var
	i: Integer;
	NewExStyle: DWORD;
begin
	NewExStyle := GetWindowLong(WinControl.Handle, GWL_EXSTYLE) or WS_EX_COMPOSITED;
	SetWindowLong(WinControl.Handle, GWL_EXSTYLE, NewExStyle);

	for i := 0 to WinControl.ControlCount - 1 do
		if WinControl.Controls[i] is TWinControl then
			EnableComposited(TWinControl(WinControl.Controls[i]));
end;

procedure TPersonnelDetailsForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	// Close the form and make sure it frees itself
	Action := caFree; // Should allow it to free itself on close
	self.Release; // Sends a Release message to itself as backup
end;

procedure TPersonnelDetailsForm.FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
	var Handled: Boolean);
var
	LTopLeft, LTopRight, LBottomLeft, LBottomRight: Integer;
	LPoint: TPoint;
begin
	Handled := true;

	// First you have to get the position of the control on screen
	// as MousePos coordinates are based on the screen positions.
	LPoint := self.ScrollBox_Content.ClientToScreen(Point(0, 0));
	LTopLeft := LPoint.X;
	LTopRight := LTopLeft + self.ScrollBox_Content.Width;
	LBottomLeft := LPoint.Y;
	LBottomRight := LBottomLeft + self.ScrollBox_Content.Width;

	if (MousePos.X >= LTopLeft) and (MousePos.X <= LTopRight) and (MousePos.Y >= LBottomLeft) and (MousePos.Y <= LBottomRight) then
	begin
		// If the mouse is inside the scrollbox coordinates,
		// scroll it by setting .VertScrollBar.Position.
		self.ScrollBox_Content.VertScrollBar.Position := self.ScrollBox_Content.VertScrollBar.Position - WheelDelta;
		Handled := true;
	end;

	if FindVCLWindow(MousePos) is TComboBox then
		Handled := true;
end;

procedure TPersonnelDetailsForm.FormShow(Sender: TObject);
var
	memo: TMemo;
	tabsheet: TTabSheet;
	ii: Integer;
begin
	for ii := 0 to 7 do
	begin
		memo := TMemo.Create(self);
		memo.Align := TAlign.alClient;
		memo.ReadOnly := true;
		memo.ScrollBars := TScrollStyle.ssVertical;
		memo.ParentColor := false;

		tabsheet := TTabSheet.Create(self);
		tabsheet.InsertControl(memo);
		tabsheet.PageControl := self.pageControl_GeneralNotes;
		tabsheet.Caption := 'A New TabSheet ' + IntToStr(ii);
		tabsheet.Tag := ii;

		memo.Text := 'A New Memo ' + IntToStr(ii);
	end;

//	EnableComposited(self);

	self.ScrollBox_Content.ScrollInView(self.panel_AddressDetails);
	self.Invalidate;
end;

end.
